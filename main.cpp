#include <iostream>
#include <algorithm>
#include <vector>
#include <list>


unsigned long GetFactorial (const int theN)
{
    return (theN <= 1) ? 1 : theN * GetFactorial(theN - 1);

}

bool IsSameCombination (std::vector <unsigned int> & theCurrentGroup,
                        std::vector <unsigned int> & theNewCombination)
{

    for (unsigned int k = 0; k < theCurrentGroup.size(); ++k)
        if (theCurrentGroup.at(k) != theNewCombination.at(k))
            return false;

    return true;

}

std::vector<unsigned int> GetNextCombination(std::vector <unsigned int> & theBaseGroup,
                                             std::vector <unsigned int> & theCurrentGroup,
                                             unsigned long              & theCounter)
{

    std::vector <unsigned int> lvi_NewCombination;

    do {
        lvi_NewCombination.clear();

        std::next_permutation(theBaseGroup.begin(), theBaseGroup.end());

        ++theCounter;

        for (unsigned int k = 0; k < theCurrentGroup.size(); ++k) {
            lvi_NewCombination.push_back(theBaseGroup.at(k));

        }

    } while (theCounter < GetFactorial(theBaseGroup.size())
             && IsSameCombination(theCurrentGroup, lvi_NewCombination));

    return theCurrentGroup = lvi_NewCombination;

}

void PrintCombination (std::vector <unsigned int> & theCurrentCombination)
{

    for (unsigned int li_Value : theCurrentCombination)
        std::cout << li_Value << "\t|\t";

}

int main()
{

    std::vector <unsigned int>             lvi_BaseGroup;           /// n <- 8
    std::vector <unsigned int>             lvi_CurrentCombination;  /// k <- 5
    unsigned long                          ll_Counter = 0;          /// 0 .. 40319 <- (8! - 1)
    std::list< std::vector<unsigned int> > llvi_CombinationList;    /// n! / (n - k)!: 6720 <- 8! / (8 - 5)!

    /// initialise base group
    lvi_BaseGroup.clear();
    lvi_BaseGroup.push_back(1);
    lvi_BaseGroup.push_back(2);
    lvi_BaseGroup.push_back(3);
    lvi_BaseGroup.push_back(4);
    lvi_BaseGroup.push_back(5);
    lvi_BaseGroup.push_back(6);
    lvi_BaseGroup.push_back(7);
    lvi_BaseGroup.push_back(8);

    /// initialise current combination
    lvi_CurrentCombination.clear();
    lvi_CurrentCombination.push_back(1);
    lvi_CurrentCombination.push_back(2);
    lvi_CurrentCombination.push_back(3);
    lvi_CurrentCombination.push_back(4);
    lvi_CurrentCombination.push_back(5);

    /// clear combination List
    llvi_CombinationList.clear();

    while (ll_Counter < GetFactorial(8)) {

        llvi_CombinationList.push_back(GetNextCombination(lvi_BaseGroup, lvi_CurrentCombination, ll_Counter));
        PrintCombination(lvi_CurrentCombination);
        std::cout << "\tCounter:\t" << ll_Counter << std::endl;

    }

    std::cout << llvi_CombinationList.size() << std::endl; /// 6720 Ofc

    return 0;
}
